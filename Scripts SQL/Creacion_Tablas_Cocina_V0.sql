﻿-- Script de creación de la base de datos del proyecto final de Jorge Pardo Pacheco


  -- Eliminamos, creamos y usamos

DROP DATABASE IF EXISTS cocina;
CREATE DATABASE cocina;

USE cocina;


-- Creamos la tabla correspondiente a las comandas
CREATE OR REPLACE TABLE comandas(
   id int AUTO_INCREMENT,
   fecha date,
   precio_total dec(5,2),
   id_plato int,
   cantidad int,
   PRIMARY KEY (id)
); 


-- Creamos la tabla correspondiente a los platos
CREATE OR REPLACE TABLE platos(
  id int AUTO_INCREMENT,
  nombre varchar(200),
  precio_publico dec(5,2),
  coste dec (5,2),
  PRIMARY KEY (id)
);


-- Creamos la tabla correspondiente a las guarniciones
CREATE OR REPLACE TABLE guarniciones(
  id int AUTO_INCREMENT,
  nombre varchar(200),
  coste dec (5,2),
  PRIMARY KEY (id)
);


-- Creamos la tabla proveedores
CREATE OR REPLACE TABLE proveedores(
  nif varchar(9),
  nombre varchar(150),
  PRIMARY KEY (nif)
);


-- Creamos la tabla pedidos
CREATE OR REPLACE TABLE pedidos(
  id int AUTO_INCREMENT,
  fecha date,
  nif varchar(9),
  descuento int,
  PRIMARY KEY (ID)
);


-- Creamos la tabla correspondiente a los productos
CREATE OR REPLACE TABLE productos(
  id int AUTO_INCREMENT,
  stock dec (7,2),
  nombre varchar (200),
  id_pedido int,
  precio_compra dec(5,2),
  PRIMARY KEY (id)
);



-- Creamos la tabla de la relación entre platos y comandas
CREATE OR REPLACE TABLE platos_r_comandas(
  id int AUTO_INCREMENT,
  id_plato int,
  id_comanda int,
  cantidad int,
  PRIMARY KEY (id)
);


-- Creamos la tabla de la relación entre platos y productos
CREATE OR REPLACE TABLE platos_r_productos(
  id int AUTO_INCREMENT,
  id_plato int,
  id_producto int,
  gramos_producto dec(7,2),
  PRIMARY KEY (id)
);


-- Creamos la tabla de la relación entre platos y guarniciones
CREATE OR REPLACE TABLE platos_r_guarniciones(
  id int AUTO_INCREMENT,
  id_plato int,
  id_guarnicion int,
  PRIMARY KEY(id)
);


-- Creamos la tabla de la relación entre guarniciones y productos
CREATE OR REPLACE TABLE guarniciones_r_productos(
  id int AUTO_INCREMENT,
  id_guarnicion int,
  id_producto int,
  gramos_producto dec(7,2),
  PRIMARY KEY (id)
);

-- Creamos la tabla correspondiente a la realción entre productos y proveedores
CREATE OR REPLACE TABLE productos_r_proveedores(
  id int AUTO_INCREMENT,
  id_producto int,
  nif varchar(9),
  precio_compra dec(5,2),
  PRIMARY KEY (id)
);

-- Creacion de la tabla teléfonos, correspondiente a un atributo multivaluado
CREATE OR REPLACE TABLE telefonos_proveedores(
  nif varchar(9),
  telefono int,
  PRIMARY KEY(nif,telefono)
  );



-- Añadimos las restricciones a las tablas

-- resticciones de la tabla platos
ALTER TABLE comandas
  ADD CONSTRAINT fk_comandas_platos
  FOREIGN KEY (id_plato)
  REFERENCES platos(id);

-- resticciones de la tabla pedidos
ALTER TABLE pedidos
  ADD CONSTRAINT fk_pedidos_proveedores
  FOREIGN KEY (nif)
  REFERENCES proveedores(nif);

-- resticciones de la tabla productos
ALTER TABLE productos
  ADD CONSTRAINT fk_productos_pedidos
  FOREIGN KEY (id_pedido)
  REFERENCES pedidos(id);

-- resticciones de la tabla de relación entre platos y comandas
ALTER TABLE platos_r_comandas
  ADD CONSTRAINT fk_plato_r_platos_comandas
  FOREIGN KEY (id_plato)
  REFERENCES platos(id),

  ADD CONSTRAINT fk_comanda_r_platos_comandas
  FOREIGN KEY (id_comanda)
  REFERENCES comandas(id),

  ADD CONSTRAINT uk_platos_r_comandas
  UNIQUE KEY  (id_plato,id_comanda);

-- resticciones de la tabla de relación entre platos y productos
ALTER TABLE platos_r_productos
  ADD CONSTRAINT fk_plato_r_platos_productos
  FOREIGN KEY (id_plato)
  REFERENCES platos(id),

  ADD CONSTRAINT fk_producto_r_platos_productos
  FOREIGN KEY (id_producto)
  REFERENCES productos(id),

  ADD CONSTRAINT uk_platos_r_productos
  UNIQUE KEY  (id_plato,id_producto);

-- resticciones de la tabla de relación entre platos y guarniciones
ALTER TABLE platos_r_guarniciones
  ADD CONSTRAINT fk_plato_r_platos_guarniciones
  FOREIGN KEY (id_plato)
  REFERENCES platos(id),

  ADD CONSTRAINT fk_guarnicion_r_platos_guarniciones
  FOREIGN KEY (id_guarnicion)
  REFERENCES guarniciones(id),

  ADD CONSTRAINT uk_platos_r_guarniciones
  UNIQUE KEY(id_plato,id_guarnicion);

-- resticciones de la tabla de relación entre guarniciones y productos
ALTER TABLE guarniciones_r_productos
  ADD CONSTRAINT fk_guarnicion_r_guaniciones_productos
  FOREIGN KEY (id_guarnicion)
  REFERENCES guarniciones(id),

  ADD CONSTRAINT productos_r_guarniciones_productos
  FOREIGN KEY (id_producto)
  REFERENCES productos(id),

  ADD CONSTRAINT uk_guarniciones_r_productos
  UNIQUE KEY  (id_guarnicion,id_producto);

-- resticciones de la tabla de relación entre productos y proveedores
ALTER TABLE productos_r_proveedores
  ADD CONSTRAINT fk_producto_r_productos_proveedores
  FOREIGN KEY (id_producto)
  REFERENCES productos(id),

  ADD CONSTRAINT fk_proveedor_r_productos_proveedores
  FOREIGN KEY (nif)
  REFERENCES proveedores(nif),

  ADD CONSTRAINT uk_productos_r_proveedores
  UNIQUE KEY(id_producto,nif);

-- restricciones de la tabla teléfonos
ALTER TABLE telefonos_proveedores
  ADD CONSTRAINT fk_telefonos_proveedores
  FOREIGN KEY (nif)
  REFERENCES proveedores(nif);




