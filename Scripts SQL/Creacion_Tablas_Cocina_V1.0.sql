﻿-- Script de creación de la base de datos del proyecto final de Jorge Pardo Pacheco


-- Eliminamos, creamos y usamos

DROP DATABASE IF EXISTS cocina;

CREATE DATABASE cocina DEFAULT CHARSET utf8 COLLATE utf8_spanish_ci;

USE cocina;


-- Creamos la tabla correspondiente a las comandas
CREATE OR REPLACE TABLE comandas (
  id int(11) NOT NULL AUTO_INCREMENT,
  fecha date DEFAULT NULL,
  precio_total decimal(5, 2) DEFAULT 0,
  id_plato int(11) DEFAULT NULL,
  cantidad int(11) DEFAULT 0,
  PRIMARY KEY (id)
); 


-- Creamos la tabla correspondiente a los platos
CREATE OR REPLACE TABLE platos(
  id int NOT NULL AUTO_INCREMENT,
  nombre varchar(200) NOT NULL,
  categoria varchar(200) DEFAULT NULL,
  precio_publico dec(5,2) DEFAULT 0,
  coste dec (5,2) DEFAULT 0,
  PRIMARY KEY (id)
);


-- Creamos la tabla correspondiente a las guarniciones
CREATE OR REPLACE TABLE guarniciones(
  id int NOT NULL AUTO_INCREMENT,
  nombre varchar(200) NOT NULL,
  coste dec (5,2) DEFAULT 0,
  PRIMARY KEY (id)
);


-- Creamos la tabla proveedores
CREATE OR REPLACE TABLE proveedores(
  id int NOT NULL AUTO_INCREMENT,
  nif varchar(9) DEFAULT NULL,
  nombre varchar(150) NOT NULL,
  PRIMARY KEY (id)
);


-- Creamos la tabla pedidos
CREATE OR REPLACE TABLE pedidos(
  id int NOT NULL AUTO_INCREMENT,
  fecha date DEFAULT NULL,
  id_proveedor int,
  descuento int DEFAULT 0,
  PRIMARY KEY (id)
);


-- Creamos la tabla correspondiente a los productos
CREATE OR REPLACE TABLE productos(
  id int NOT NULL AUTO_INCREMENT,
  stock dec (7,2) DEFAULT 0,
  nombre varchar (200) DEFAULT NULL,
  id_pedido int,
  precio_compra dec(5,2) DEFAULT 0,
  PRIMARY KEY (id)
);



-- Creamos la tabla de la relación entre platos y comandas
CREATE OR REPLACE TABLE platos_r_comandas(
  id int NOT NULL AUTO_INCREMENT,
  id_plato int,
  id_comanda int,
  cantidad int DEFAULT 0,
  PRIMARY KEY (id)
);


-- Creamos la tabla de la relación entre platos y productos
CREATE OR REPLACE TABLE platos_r_productos(
  id int NOT NULL AUTO_INCREMENT,
  id_plato int,
  id_producto int,
  gramos_producto dec(7,2) DEFAULT 0,
  PRIMARY KEY (id)
);


-- Creamos la tabla de la relación entre platos y guarniciones
CREATE OR REPLACE TABLE platos_r_guarniciones(
  id int NOT NULL AUTO_INCREMENT,
  id_plato int,
  id_guarnicion int,
  PRIMARY KEY(id)
);


-- Creamos la tabla de la relación entre guarniciones y productos
CREATE OR REPLACE TABLE guarniciones_r_productos(
  id int NOT NULL AUTO_INCREMENT,
  id_guarnicion int,
  id_producto int,
  gramos_producto dec(7,2) DEFAULT 0,
  PRIMARY KEY (id)
);

-- Creamos la tabla correspondiente a la realción entre productos y proveedores
CREATE OR REPLACE TABLE productos_r_proveedores(
  id int NOT NULL AUTO_INCREMENT,
  id_producto int,
  id_proveedor int,
  precio_compra dec(5,2) DEFAULT 0,
  PRIMARY KEY (id)
);

-- Creacion de la tabla teléfonos, correspondiente a un atributo multivaluado
CREATE OR REPLACE TABLE telefonos_proveedores(
  id int NOT NULL AUTO_INCREMENT,
  id_proveedor int,
  telefono int,
  PRIMARY KEY(id)
  );



-- Añadimos las restricciones a las tablas

-- resticciones de la tabla platos
ALTER TABLE comandas
  ADD CONSTRAINT fk_comandas_platos
  FOREIGN KEY (id_plato)
  REFERENCES platos(id) ON DELETE NO ACTION ON UPDATE CASCADE;

-- resticciones de la tabla pedidos
ALTER TABLE pedidos
  ADD CONSTRAINT fk_pedidos_proveedores
  FOREIGN KEY (id_proveedor)
  REFERENCES proveedores(id) ON DELETE NO ACTION ON UPDATE CASCADE;

-- resticciones de la tabla productos
ALTER TABLE productos
  ADD CONSTRAINT fk_productos_pedidos
  FOREIGN KEY (id_pedido)
  REFERENCES pedidos(id) ON DELETE NO ACTION ON UPDATE CASCADE;

-- resticciones de la tabla de relación entre platos y comandas
ALTER TABLE platos_r_comandas
  ADD CONSTRAINT fk_plato_r_platos_comandas
  FOREIGN KEY (id_plato)
  REFERENCES platos(id) ON DELETE NO ACTION ON UPDATE CASCADE,

  ADD CONSTRAINT fk_comanda_r_platos_comandas
  FOREIGN KEY (id_comanda)
  REFERENCES comandas(id) ON DELETE NO ACTION ON UPDATE CASCADE,

  ADD CONSTRAINT uk_platos_r_comandas
  UNIQUE KEY  (id_plato,id_comanda);

-- resticciones de la tabla de relación entre platos y productos
ALTER TABLE platos_r_productos
  ADD CONSTRAINT fk_plato_r_platos_productos
  FOREIGN KEY (id_plato)
  REFERENCES platos(id) ON DELETE CASCADE ON UPDATE CASCADE,

  ADD CONSTRAINT fk_producto_r_platos_productos
  FOREIGN KEY (id_producto)
  REFERENCES productos(id) ON DELETE CASCADE ON UPDATE CASCADE,

  ADD CONSTRAINT uk_platos_r_productos
  UNIQUE KEY  (id_plato,id_producto);

-- resticciones de la tabla de relación entre platos y guarniciones
ALTER TABLE platos_r_guarniciones
  ADD CONSTRAINT fk_plato_r_platos_guarniciones
  FOREIGN KEY (id_plato)
  REFERENCES platos(id) ON DELETE CASCADE ON UPDATE CASCADE,

  ADD CONSTRAINT fk_guarnicion_r_platos_guarniciones
  FOREIGN KEY (id_guarnicion)
  REFERENCES guarniciones(id) ON DELETE CASCADE ON UPDATE CASCADE,

  ADD CONSTRAINT uk_platos_r_guarniciones
  UNIQUE KEY(id_plato,id_guarnicion);

-- resticciones de la tabla de relación entre guarniciones y productos
ALTER TABLE guarniciones_r_productos
  ADD CONSTRAINT fk_guarnicion_r_guaniciones_productos
  FOREIGN KEY (id_guarnicion)
  REFERENCES guarniciones(id) ON DELETE CASCADE ON UPDATE CASCADE,

  ADD CONSTRAINT productos_r_guarniciones_productos
  FOREIGN KEY (id_producto)
  REFERENCES productos(id) ON DELETE CASCADE ON UPDATE CASCADE,

  ADD CONSTRAINT uk_guarniciones_r_productos
  UNIQUE KEY  (id_guarnicion,id_producto);

-- resticciones de la tabla de relación entre productos y proveedores
ALTER TABLE productos_r_proveedores
  ADD CONSTRAINT fk_producto_r_productos_proveedores
  FOREIGN KEY (id_producto)
  REFERENCES productos(id) ON DELETE CASCADE ON UPDATE CASCADE,

  ADD CONSTRAINT fk_proveedor_r_productos_proveedores
  FOREIGN KEY (id_proveedor)
  REFERENCES proveedores(id) ON DELETE CASCADE ON UPDATE CASCADE,

  ADD CONSTRAINT uk_productos_r_proveedores
  UNIQUE KEY(id_producto,id_proveedor);

-- restricciones de la tabla teléfonos
ALTER TABLE telefonos_proveedores
  ADD CONSTRAINT fk_telefonos_proveedores
  FOREIGN KEY (id_proveedor)
  REFERENCES proveedores(id) ON DELETE CASCADE ON UPDATE CASCADE;




